
$(document).ready(function() {
    filter();
    check_button();
});

$("#tagset").on("change", function() {
    filter();
});

$("#burst").on("change", function() {
    filter();
});

$('input:checkbox').change(function() {
    check_button();
});


function filter() {
    var tagset = $("#tagset").val();
    var burst = $("#burst").val();

    $("#users div").removeAttr('hidden');
    $("#users div").not(".tagset_" + tagset).attr("hidden", true);
    $("#users div").not(".burst_" + burst).attr("hidden", true);
    check_button();
}

function check_button() {
   if (($("#users div:visible").children("input:checkbox:checked").length) < 1) {
        $("#generate-button").attr('disabled', true);
   } else {
       $("#generate-button").removeAttr('disabled');
   }
}



// on document load start filter
// disable generate button if no checkbox is set