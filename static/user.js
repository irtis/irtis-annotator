
$(document).ready(function() {

    $("input:checkbox").change(function() {
        var val = $(this).attr("value");
        var is_checked = $(this).is(':checked')

        $("tr").each(function() {
            if ($(this).children("td.status").text() == val) {
                if (is_checked) {
                    $(this).removeAttr("hidden");
                } else {
                    $(this).attr("hidden", true);
                }
            }
        });
    });
});
