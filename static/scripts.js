
$(document).ready(function() {

    //Increment the idle time counter every minute
    var idleInterval = setInterval(timerIncrement, 1000); // 1 second

    function timerIncrement() {
        idleTime++;
        if (idleTime > 300) {  // redirection is set to 5 minutes
            $("#stop_button").click();
        }
    }
    // Zero the idle timer on mouse movement.
    $(this).mousemove(function(e){
      idleTime = 0;
    });
    $(this).keypress(function (e) {
        idleTime = 0;
    });

    // hide selects in rows without annotation
    $('input.annotation').filter(function(){
        return $(this).val().trim();
    }).parent().children("select").removeAttr('hidden');

    hideOption($('select.annotation'));
    hideSelect($('td.annotation'));


    $("table tbody tr th").first().click(); // on load select first line

});


$(document).keyup(function(e) {
    if (e.which == 8 && !$("tr.highlighted").is(':first-child')) { // go back on backspace key
        highlight_row($("tr.highlighted").prev());
    }

    if (e.which == 9) { // change focus on tab
        e.preventDefault();
        $("#enlarge-button").focus();
    }
});


$(document).keyup(function(e) { // on shift scroll in manual
    // filter inappropriate keys
    var button_length = $("div#upper-buttons button").length; // number of buttons
    var code = e.which;
    if (e.shiftKey && code >= 48) {
        if (code < 47 + button_length + 1) {
            e.preventDefault();
            var place = code - 48

            if (place == 0) {  // 0 for reset
                $("#manual").children().removeAttr("hidden");
            } else {
                $("#manual h2, #manual h3, #manual h4, #manual ul").attr("hidden", true);
                $("#manual").children("p").attr("hidden", true);
                element = $("#manual h4").eq(place);
                element.removeAttr("hidden");
                element.nextUntil("h4").removeAttr("hidden");
            }
        }
    }
});


// Add annotation by keypress to highlighted row and move to another
$(document).keypress(function(e) {
    var code = e.which;

    // filter inappropriate keys
    if (e.shiftKey) {
        return;
    }
    var button_length = $("div#upper-buttons button").length; // number of buttons
    if (code <= 47 || code > 47 + button_length) {
        return;
    }

    // give annotation to annotation column and highlight next row
    var tag_text = $("div#upper-buttons button:nth-child(" + (code - 47) + ")").attr("value");
    process_annotation(tag_text)
});


// Fill select box in by pressing Z, P, K, U, ?
$(document).keypress(function(e) {
    var code = e.which;

    if (code == 90 || code == 80 || code == 75 || code == 85 || code == 63 ||
            code == 122 || code == 112 || code == 107 || code == 117) {
        var char = String.fromCharCode(code).toUpperCase()
        if (code == 63) {
            char = "\\?"
        }

        var el = $("tr.highlighted").prev().find("select")

        if (el.find("option[value=" + char + "]").length > 0) {
            el.find("option").removeAttr('selected');
            el.find("option[value=" + char + "]").attr('selected','selected');
        }
    }
});

// Add annotation by button click to highlighted row and move to another
$("div#upper-buttons button").click(function() {
    var tag_text = $(this).attr("value");
    process_annotation(tag_text)
});

// Highlight row in table based on users click
$("#data tbody tr th, #main tr td:not(.annotation)").click(function() {
    highlight_row($(this).parent())
});

$("#hide-button").click(function() {
    $("#right-container").attr("hidden", true);
    $("#left-container").removeClass("col-lg-9");
    $("#left-container").addClass("col-lg-12");
    $("input[name='is-hidden'").val("True");
    $("#show-button").removeAttr("hidden");
});

$("#show-button").click(function() {
    $("#right-container").removeAttr("hidden");
    $("#left-container").removeClass("col-lg-12");
    $("#left-container").addClass("col-lg-9");
    $("input[name='is-hidden'").val("False");
    $("#show-button").attr("hidden", true);
});

$("#enlarge-button").click(function() {
    if ($(this).text() == "Enlarge") {
        $(this).text("Reduce");
        $("#left-container").attr("hidden", true);
        $("#right-container").removeClass("col-lg-3");
        $("#right-container").addClass("col-lg-12");
        $("#hide-button").attr("hidden", true);
    }
    else {
        $(this).text("Enlarge");
        $("#left-container").removeAttr("hidden");
        $("#right-container").removeClass("col-lg-12");
        $("#right-container").addClass("col-lg-3");
        $("#hide-button").removeAttr("hidden");
    }
});


$("#table-form").submit(function(e) {
    var button = $(this).find("button[type=submit]:focus").val();
    if (button == "save") {
        var empty = $('input').filter(function(){ return !$(this).val().trim();}).length;

        if (empty > 0) {
            if (confirm('Some tags are not filled. Are you sure to continue?')) {
                return true;
            } else {
                return false;
            }
        }
    }
    return true;
});

$("#previous-button").click(function() {
    $("table tbody:visible").prev("tbody").removeAttr('hidden');
    $("#reset-button").removeAttr('disabled');

    if ($("table tbody.prev_data").length == $("table tbody.prev_data:visible").length) {
        $("#previous-button").attr('disabled', true);
    }
});

$("#reset-button").click(function() {
    $("tbody.prev_data").attr("hidden", true);
    $("tbody.follow_data").attr("hidden", true);
    $("#reset-button").attr('disabled', true);
    if ($("table tbody.prev_data").length > 0) {
        $("#previous-button").removeAttr('disabled');
    }
    if ($("table tbody.follow_data").length > 0) {
        $("#following-button").removeAttr('disabled');
    }
});

$("#following-button").click(function() {
    $("table tbody:visible").next("tbody").removeAttr('hidden');
    $("#reset-button").removeAttr('disabled');

    if ($("table tbody.follow_data").length == $("table tbody.follow_data:visible").length) {
        $("#following-button").attr('disabled', true);
    }
});


// Highlight row in table
function highlight_row(element) {
    if (!element.is("tr")) { // keep highlighted element inside the table
        return;
    }

    var is_highlighted = element.hasClass("highlighted");
    $("#data tr").removeClass("highlighted");  // deselect already highlighted
    element.addClass("highlighted");

    // keep focus on third element above currently investigated element
    var topElement = element;
    for (i = 0; i < 4; i++) {
        if (topElement.prev()) {
            topElement = topElement.prev();
        }
    }
    var rowpos = topElement.position();
    $('#table-container').scrollTop(rowpos.top);
}

// Hide not needed options and possibly select
function hideOption(element) {
    element.children('option').each(function(){
        var myClass = $(this).attr("class");
        var myId = myClass.substring(myClass.lastIndexOf(" ") + 1);
        var myTag = $("#" + myId).attr("value")
        var selectTag = $(this).parent().parent().children("input").attr("value")

        if (myTag != selectTag) {
            $(this).attr("hidden", true);
        } else {
            $(this).removeAttr('hidden');
        }
    });
}

function hideSelect(element) {
    element.children('select').each(function() {
        if ($(this).children("[hidden!='hidden']").length == 0) {
            $(this).attr("hidden", true);
        }
    });
}

function process_annotation(tag_text) {
    $("tr.highlighted td.annotation input").attr("value", tag_text);
    $("tr.highlighted td.annotation select").removeAttr('hidden');
    $("tr.highlighted td.annotation select").val("");
    // $("tr.highlighted td.annotation select option").removeAttr('selected');
    hideOption($("tr.highlighted td.annotation select"));
    hideSelect($("tr.highlighted td.annotation"));

    if ($("tr.highlighted").is(":last-child")) {
        $("#save_data_button").focus();
        return;
    }

    highlight_row($("tr.highlighted").next());
}

