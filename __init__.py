from flask import Flask, render_template, redirect, url_for, request, \
    send_from_directory
from os import path, access, R_OK, unlink, walk, listdir, linesep
from pathlib import Path
from shutil import copy2
import json
from openpyxl import Workbook
from markdown import markdown
from datetime import datetime, timedelta
from typing import Union, Dict, Any, List, Tuple
from zipfile import ZipFile


app = Flask(__name__)

DIR = "/var/www/webApp/webApp/"
# DIR = ""
ANNOTATION_DIR = path.join(DIR, "annotations")
JSON_DIR = path.join(DIR, "json")
DATA_DIR = path.join(DIR, "data")
DOWNLOAD_DIR = path.join(DIR, "static", "download")

ANNOTATION_CSV = "annotation_only.csv"
ANNOTATION_XLSX = "textual_annotations.xlsx"

SURROUNDING_LIMIT = 10
NO_TAG = "NO TAG"
EXTENDED_TAG = "ext_"

SUPERUSER_NAME = "ae20150db78f58cd"

CONVERSATION_INDEX_PROMPT = "Conversation index out of range."
MISSING_USER_DB_PROMPT = "Sorry. User not in database. Please check the " \
                         "username in url."
BAD_GENERATION_REQUEST = "Request for xlsx files generation could not be " \
                         "processed. Please try it again from previous page."
PERMISSION_ERROR = "File cannot be generated due to permission error. Please" \
                   " try it later."

UNFINISHED_STATUS = "Unfinished"
FINISHED_STATUS = "Finished"
ACTIVE_STATUS = "Active"
STATUSES = (ACTIVE_STATUS, UNFINISHED_STATUS, FINISHED_STATUS)


class UtilsJson:
    """ Json file operation. """
    BACKUP_DIR = "backup"
    BACKUP_FILE = "backup.json"
    BACKUP_DELTA = 3600

    def __init__(self, my_dir, json_dir, annotation_dir, download_dir):
        SOURCE_ANNOTATIONS = "messenger_annotation.json"
        ASSIGNMENTS = "assignments.json"
        TAG_SETS = "tagsets.json"
        TAG_TABLE = "tags.json"
        USERS_JSON = "users.json"

        self.json_dir = json_dir
        self.annot_dir = annotation_dir
        self.download_dir = download_dir

        self.source_annot = UtilsJson.load_json(json_dir, SOURCE_ANNOTATIONS)
        self.assignments = UtilsJson.load_json(json_dir, ASSIGNMENTS)
        self.tag_sets = UtilsJson.load_json(json_dir, TAG_SETS)
        self.tag_table = UtilsJson.load_json(json_dir, TAG_TABLE)
        self.users = UtilsJson.load_json(json_dir, USERS_JSON)

        self.backup_dir = path.join(my_dir, UtilsJson.BACKUP_DIR)
        self.backup_path = path.join(self.backup_dir, UtilsJson.BACKUP_FILE)

        if not path.isfile(self.backup_path) or not access(
                self.backup_path, R_OK):
            with open(self.backup_path, "w", encoding="utf-8") as json_file:
                json_file.write(json.dumps({}))
        self.backup = UtilsJson.load_json(
            path.join(my_dir, UtilsJson.BACKUP_DIR), UtilsJson.BACKUP_FILE)

    def load_data_json(self, user, assignment):
        """Return file with data."""
        return self.load_json(self.annot_dir, user + "_" + assignment + ".json")

    def get_output_json_path(
            self, user: str, assignment: str) -> Union[str, Path]:
        """Return path for file with annotated data."""
        return path.join(self.annot_dir, user + "_" + assignment + ".json")

    @staticmethod
    def load_json(directory: Union[str, Path], filename: str) -> Any:
        """Load json file."""
        file_path = path.join(directory, filename)
        with open(file_path, "r", encoding="utf-8") as json_file:
            return json.load(json_file)

    def save_json(self, json_data: Any, user: str, assignment: str) -> None:
        """Save json file with annotated data."""
        with open(self.get_output_json_path(user, assignment), "w",
                  encoding="utf-8") as json_file:
            json.dump(json_data, json_file)
        self.create_backup()

    @staticmethod
    def load_manual(directory: Union[str, Path], filename: str) -> Any:
        """Load markdown manual."""
        file_path = path.join(directory, filename)
        with open(file_path, encoding="utf-8") as md_file:
            return markdown("\n\n".join(md_file.read().split("\n")))

    def create_backup(self, take_all: bool = False):
        """Create backup to json file."""
        def get_path(index: str):
            return path.join(self.backup_dir, "backup_{}.zip".
                             format(index))

        my_id, last_time = self.get_last_backup_info()
        my_now = round(datetime.now().timestamp(), 1)
        diff = my_now - last_time
        if my_id and diff <= UtilsJson.BACKUP_DELTA and not take_all:
            return

        folder = path.join(self.download_dir)
        del_path = path.join(folder, "backup_{}.zip".format(my_id))
        if path.isfile(del_path) or path.islink(del_path):
            unlink(del_path)

        my_id = "0" if not my_id else str(int(my_id) + 1)
        with ZipFile(get_path(my_id), "w") as zipf:
            for file in self.get_all_file_paths(my_id == "0" or take_all):
                zipf.write(file)
        copy2(get_path(my_id), self.download_dir)

        self.backup[my_id] = my_now
        with open(self.backup_path, "w", encoding="utf-8") as back_file:
            json.dump(self.backup, back_file)

    def get_all_file_paths(self, take_all: bool = False):
        """Get all json files to be stored."""
        file_paths = []
        for root, directories, files in walk(self.annot_dir):
            for filename in files:
                file_path = path.join(root, filename)
                filename = Path(file_path)
                diff = datetime.now().timestamp() - filename.stat().st_mtime
                if take_all or diff < UtilsJson.BACKUP_DELTA:
                    file_paths.append(file_path)

        return file_paths

    def get_last_backup_info(self):
        """Get index and time when last backup was made."""
        if not self.backup:
            return None, round(datetime.now().timestamp(), 1)
        max_key = max(self.backup, key=int)
        return max_key, self.backup[str(max_key)]


class UtilsStats:
    """ Statistics utilities. """

    ANNOTATION_LINE_LIMIT = 10
    ANNOTATION_LINE_SPLIT = 75
    ANNOTATION_LINE_MULTIPLIER = 4

    @staticmethod
    def count_annotated_lines(data: Dict[Any, Any], maximum: int) -> int:
        """Count number of annotated lines in conversation."""
        counter = 0
        for i in range(1, maximum + 1):
            if str(i) in data and data[str(i)] != "":
                counter += 1
        return counter

    @staticmethod
    def count_time(starting_times: List[float],
                   finish_times: List[float], lines: int) -> float:
        """Count the time for which the conversation was annotated."""
        start_lst = starting_times[:]
        end_lst = finish_times[:]
        limit = UtilsStats.ANNOTATION_LINE_LIMIT * lines
        if lines <= UtilsStats.ANNOTATION_LINE_SPLIT:
            limit *= UtilsStats.ANNOTATION_LINE_MULTIPLIER

        if not end_lst:
            return 0.0

        output = 0.0
        while end_lst:
            end = end_lst.pop()
            if not start_lst:
                return output
            start = start_lst.pop()
            while end - start < 0:
                if not start_lst:
                    return output
                start = start_lst.pop()
            if end - start <= limit or lines == 0:
                output += end - start

        return round(output, 1)

    @staticmethod
    def count_stats(json_files: UtilsJson, user: str,
                    assignment: str) -> Tuple[Any, Any]:
        """Count statistics about assignment."""
        data = json_files.load_data_json(user, assignment)["conversations"]
        burst = json_files.assignments[user][assignment][0]
        annot = json_files.source_annot[burst]

        output = []
        counters = [0, 0, 0, 0, 0.0, 0]
        for i in range(len(annot)):
            key = str(i)
            record: Any = []
            record.extend((key, annot[i][0], annot[i][1], annot[i][2]))
            if key in data:
                lines = UtilsStats.count_annotated_lines(
                    data[key][0], annot[i][4])
                time = UtilsStats.count_time(data[key][1], data[key][2], lines)
                counters[0] += len(data[key][1])
                counters[1] += len(data[key][2])
                counters[2] += lines
                counters[4] += time
                counters[5] += 1 if annot[i][4] == lines else 0
                record.extend((len(data[key][1]), len(data[key][2]), lines,
                               annot[i][4], round(time, 1),
                               annot[i][4] == lines))
            else:
                record.extend([0, 0, 0, annot[i][4], "0", False])
            counters[3] += annot[i][4]
            output.append(record)

        footer = ("", "", "", "", counters[0], counters[1], "{}/{}".format(
            counters[2], counters[3]), timedelta(seconds=round(counters[4])),
                  "{}/{}".format(counters[5], len(annot)))

        return output, footer

    @staticmethod
    def return_empty(json_files: UtilsJson, user: str, assignment: str) -> Any:
        not_annotated = []
        output, _ = UtilsStats.count_stats(json_files, user, assignment)
        for entry in output:
            if not entry[9]:
                not_annotated.append(entry)
        return not_annotated


class Utils:
    @staticmethod
    def get_prompt(prompt: str) -> str:
        """Return string as HTML element."""
        return "<h1>{}</h1>".format(prompt)

    @staticmethod
    def get_ids(json_file: Any, burst: str,
                conversation: int) -> Tuple[str, str, str]:
        """Get ids of user, thread and conversation."""
        return json_file[burst][conversation][0], \
               json_file[burst][conversation][1], \
               json_file[burst][conversation][2]

    @staticmethod
    def get_conv_limit(my_dir: str, user: str, thread: str) -> int:
        """Return number of available conversation in thread."""
        return len([name for name in listdir(path.join(my_dir, user, thread))])

    @staticmethod
    def get_conv_string(data_dir: str, json_file: Any, burst: str,
                        conversation: int, modifier: int = 0) -> List[str]:
        """Return all textual lines of requested conversation."""
        user_id, thread, convers = Utils.get_ids(
            json_file, burst, int(conversation))
        conv = int(convers) + modifier
        if conv < 0 or conv >= Utils.get_conv_limit(data_dir, user_id, thread):
            return []

        my_path = path.join(data_dir, user_id, thread, str(conv) + ".csv")
        with open(my_path, "r", encoding="utf-8") as conv_file:
            return conv_file.readlines()

    @staticmethod
    def get_first_unfinished(json_files: UtilsJson, data_json: Dict[Any, Any],
                             assignment: str) -> str:
        """Get index of first unfinished annotation. """
        annot = json_files.source_annot[assignment]
        for i in range(len(annot)):
            if not str(i) in data_json["conversations"] or \
                    not data_json["conversations"][str(i)][2]:
                if i > 0:
                    # test if previous is not fully annotated
                    lines = UtilsStats.count_annotated_lines(
                        data_json["conversations"][str(i - 1)][0],
                        annot[i - 1][4])
                    return str(i) if lines == annot[i - 1][4] else str(i - 1)
                return str(i)
        return "0"  # in case all are finished

    @staticmethod
    def get_tags(json_files: UtilsJson, tagset: str,
                 tag_json: Dict[Any, Any]) -> List[Tuple[str, str, List[str]]]:
        """Return list tags with their id and subtags."""
        tag_table = json_files.tag_table
        tag_ids_lst = [lst for lst in tag_json[tagset]["tagset"]]
        tag_ids: List[Tuple[str, str, List[str]]] = []
        for dct in tag_ids_lst:
            for key, value in dct.items():
                tag_ids.append((key, tag_table[key], []))
                for my_id in value:
                    tag_ids[-1][2].append(tag_table[my_id])
        return tag_ids

    @staticmethod
    def get_tag_id(json_files: UtilsJson, target_desc: str) -> Any:
        """Return appropriate tag in for given tag decription."""
        tag_table = json_files.tag_table
        for tag_id, tag_desc in tag_table.items():
            if target_desc == tag_desc:
                return tag_id
        return ""

    @staticmethod
    def get_tag_name(json_files: UtilsJson, tag: str) -> Any:
        """Return appropriate tag name for given tag id."""
        if tag == "0":
            return ""
        if tag in JSON_FILES.tag_table:
            return JSON_FILES.tag_table[tag]
        return ""

    @staticmethod
    def get_status(json_files: UtilsJson, user: str,
                   assignment: str, active: str,
                   finished: str) -> Tuple[str, str, str]:
        """Return status for given conversation."""
        if path.exists(json_files.get_output_json_path(user, assignment)):
            data = json_files.load_data_json(user, assignment)
            if "conversations" in data and "0" in data["conversations"] \
                    and "status" in data:
                start_time = "NaN"
                if data["conversations"]["0"] and \
                        data["conversations"]["0"][1]:
                    dt_obj = datetime.fromtimestamp(
                        data["conversations"]["0"][1][0])
                    start_time = dt_obj.strftime("%d/%m/%Y, %H:%M:%S")
                if data["status"] == finished:
                    finish_time = "NaN"
                    if "finished_time" in data:
                        dt_obj = datetime.fromtimestamp(data["finished_time"])
                        finish_time = dt_obj.strftime("%d/%m/%Y, %H:%M:%S")
                    return data["status"], start_time, finish_time

                return data["status"], start_time, "NaN"
            return active, "NaN", "NaN"
        return active, "NaN", "NaN"

    @staticmethod
    def get_strings(data_dir: str, annot_json: Any, burst: str, ind: int,
                    limit: int) -> Any:
        previous_strings = []
        for i in range(-limit, 0):
            string = Utils.get_conv_string(data_dir, annot_json, burst, ind, i)
            if string:
                previous_strings.append(string)
        result_string = Utils.get_conv_string(data_dir, annot_json, burst, ind)
        following_strings = []
        for i in range(1, limit):
            string = Utils.get_conv_string(data_dir, annot_json, burst, ind, i)
            if string:
                following_strings.append(string)

        return previous_strings, result_string, following_strings

    @staticmethod
    def open_record(json_files: UtilsJson, user: str, assignment: str,
                    index: str) -> Dict[str, str]:
        """Open existing record in json file, return already made annots."""
        data_json = json_files.load_data_json(user, assignment)
        id_table = json_files.tag_table

        if str(index) not in data_json["conversations"]:  # create if not exist
            # list of (dict of lines with tags, starting times, ending times)
            data_json["conversations"][str(index)] = [{}, [], []]
        data_json["conversations"][str(index)][1].append(
            round(datetime.now().timestamp(), 1))
        json_files.save_json(data_json, user, assignment)
        tags = {}  # return translated tag ids
        for key, value in data_json["conversations"][str(index)][0].items():
            if not isinstance(value, list):
                tags[key] = id_table.get(value, "")
        return tags

    @staticmethod
    def save_lines_with_tags(json_files: UtilsJson, user: str, assignment: str,
                             index: str, data: Any) -> None:
        """Save data to json file with annotation."""
        data_json = json_files.load_data_json(user, assignment)
        for line, tag in data.to_dict().items():
            data_json["conversations"][index][0][line] = Utils.get_tag_id(
                json_files, tag.strip())
        data_json["conversations"][str(index)][2].append(
            round(datetime.now().timestamp(), 1))
        json_files.save_json(data_json, user, assignment)

    @staticmethod
    def prepare_admin_data(json_files: UtilsJson, active: str,
                           finished: str) -> Any:
        output = []
        users = json_files.users["users"]
        assignments = json_files.assignments

        for user_tuple in users:
            user = user_tuple[0]
            if user in assignments:
                for assignment in assignments[user]:
                    status, start, finish = Utils.get_status(
                        json_files, user, assignment, active, finished)
                    lines, time, fully = "NaN", "NaN", "NaN"
                    if status != active:
                        _, stats = UtilsStats.count_stats(
                            json_files, user, assignment)
                        lines, time, fully = stats[6], stats[7], stats[8]
                    color = "danger" if status == active else "success" if \
                        status == finished else "warning"
                    output.append((user_tuple[1], user_tuple[0], assignment,
                                   assignments[user][assignment][2],
                                   assignments[user][assignment][0],
                                   assignments[user][assignment][1], status,
                                   color, start, finish, lines, time, fully))

        return output

    @staticmethod
    def prepare_xlsx_generation_data():
        tagset_list = []
        for tagset_id, value in JSON_FILES.tag_sets.items():
            data = Utils.get_tags(JSON_FILES, tagset_id, JSON_FILES.tag_sets)
            output = ""
            for i in range(1, min(4, len(data))):
                output += data[i][1] + ", "
            output += "..."
            tagset_list.append((tagset_id, output))

        burst_list = []
        for burst_id, value in JSON_FILES.source_annot.items():
            output = "" if not value else "{}-{}-{}, {}-{}-{}, ...".format(
                value[0][0], value[0][1], value[0][2], value[1][0], value[1][1],
                value[1][2])
            burst_list.append((burst_id, output))

        users_list = []
        for user_id, value in JSON_FILES.assignments.items():
            for assignment_id, assignment in value.items():
                status = Utils.get_status(JSON_FILES, user_id, assignment_id,
                                          ACTIVE_STATUS, FINISHED_STATUS)
                if status[0] != ACTIVE_STATUS:
                    users_list.append((user_id, Utils.get_nickname(user_id),
                                       assignment_id, assignment[0],
                                       assignment[1],
                                       status))

        return tagset_list, burst_list, users_list

    @staticmethod
    def get_nickname(user_id):
        for entry in JSON_FILES.users["users"]:
            if entry[0] == user_id:
                return entry[1]
        return ""

    @staticmethod
    def generate_xslx_files(form_data):
        def get_letter(index):
            return chr(ord('A') + index)

        def generate_header(wlist, header, users):
            for i in range(len(header)):
                wlist["{}1".format(get_letter(i))] = header[i]
            for i in range(len(users)):
                wlist["{}1".format(get_letter(len(header) + i * 2))] = users[i][0]

        def generate_body(wlist, record, ln_counter, cln_number, ln, c_id):
            wlist["A{}".format(i)] = int(record[0])
            wlist["B{}".format(i)] = int(record[1])
            wlist["C{}".format(i)] = int(record[2])
            wlist["D{}".format(i)] = c_id
            wlist["E{}".format(i)] = record[3]
            wlist["F{}".format(i)] = ln_counter
            wlist["G{}".format(i)] = cln_number
            wlist["H{}".format(i)] = ln

        def is_extended_question_mark(cline, data, conv):
            if str("{}{}".format(EXTENDED_TAG, cline)) in data[str(conv)][0]:
                return data[str(conv)][0]["{}{}".format(
                    EXTENDED_TAG, cline)] == "14"
            return False

        def generate_data(wlist, csv_in, my_users, users_js, conv, cline, my_i,
                          question_check):
            output = csv_in
            for j in range(len(my_users)):
                data = users_js[j]["conversations"]
                if str(conv) in data and data[str(conv)][0]:
                    if str(cline) in data[str(conv)][0]:
                        value = data[str(conv)][0][str(cline)]
                        wlist["{}{}".format(get_letter(8 + j * 2), my_i)] = \
                            Utils.get_tag_name(JSON_FILES, value)
                        if not value or (not question_check and
                                         is_extended_question_mark(cline, data,
                                                                   conv)):
                            output += "0;"
                        else:
                            output += value + ";"
                    else:
                        output += "0;"
                    if str("{}{}".format(EXTENDED_TAG, cline)) \
                            in data[str(conv)][0]:
                        wlist["{}{}".format(get_letter(9 + j * 2), my_i)] = \
                            Utils.get_tag_name(
                                JSON_FILES,
                                data[str(conv)][0]["{}{}".format(
                                    EXTENDED_TAG, cline)])
                else:
                    output += "0;"
            return output + "\n"

        try:
            burst = form_data["burst"]
            users_request = form_data.getlist("users")
            question_check = True if "question_check" in form_data else False
        except Exception:
            return Utils.get_prompt(BAD_GENERATION_REQUEST)

        users = []
        for entry in users_request:
            users.append(entry.split("_"))

        users_json = []
        for i in range(len(users)):
            users_json.append(JSON_FILES.load_data_json(users[i][0],
                                                        users[i][1]))

        wb = Workbook()
        ws = wb.active
        csv_out = ""
        generate_header(ws, ["user", "thread", "conversation", "conv_id",
                             "is_group", "line_number_in_file",
                             "line_number_in_conversation", "line"], users)

        i = 2
        line_counter = 1
        for conv_id, entry in enumerate(JSON_FILES.source_annot[burst]):
            my_path = path.join(DATA_DIR, entry[0], entry[1], entry[2] + ".csv")
            with open(my_path, encoding="UTF-8") as conversation_file:
                line = conversation_file.readline()
                cline_number = 1
                while line:
                    generate_body(ws, entry, line_counter, cline_number, line,
                                  conv_id)
                    csv_out = generate_data(ws, csv_out, users, users_json,
                                            conv_id, cline_number, i,
                                            question_check)

                    line = conversation_file.readline().strip()
                    i += 1
                    line_counter += 1
                    cline_number += 1

            i += 2  # skip two lines

        try:
            wb.save(path.join(DOWNLOAD_DIR, ANNOTATION_XLSX))
            with open(path.join(DOWNLOAD_DIR, ANNOTATION_CSV),
                      "w") as csv_file:
                csv_file.write(csv_out.replace(";\n", "\n"))
        except Exception:
            return Utils.get_prompt(PERMISSION_ERROR)


""" Main endpoints """
JSON_FILES = UtilsJson(DIR, JSON_DIR, ANNOTATION_DIR, DOWNLOAD_DIR)


def return_next_conversation(user: str, assignment: str, index: str,
                             new_index: str, data: Any) -> Any:
    """Load data and return next conversation for annotation."""
    Utils.save_lines_with_tags(JSON_FILES, user, assignment, index, data)
    return redirect(url_for("home", user=user, assignment=assignment,
                            index=new_index,
                            is_hidden=request.form["is-hidden"].strip()))


@app.route("/<user>", methods=["GET", "POST"])
def set_assignment(user: str) -> Any:
    users_json = JSON_FILES.users
    assignments_json = JSON_FILES.assignments
    user_list = [user for user, _ in users_json["users"]]
    if user not in user_list:
        return Utils.get_prompt(MISSING_USER_DB_PROMPT)

    assign_strings = [Utils.get_status(JSON_FILES, user, key, ACTIVE_STATUS,
                                       FINISHED_STATUS)
                      for key in assignments_json[user].keys()]
    assigned_keys = []
    for key, value in assignments_json[user].items():
        assigned_keys.append((key, value[0], value[1]))

    return render_template("user.html", user=user, assignments=assigned_keys,
                           statuses=assign_strings, all_statuses=STATUSES)


@app.route("/<user>/<assignment>", methods=["POST"])
def implicit_entry(user: str, assignment: str) -> Any:
    if assignment == "stats":
        return redirect(url_for("return_stat_data", user=request.form["user"],
                                assignment=request.form["assignment"]))

    # load json if exist, create it otherwise
    file_path = JSON_FILES.get_output_json_path(user, assignment)
    if not path.isfile(file_path) or not access(file_path, R_OK):
        with open(file_path, "w", encoding="utf-8") as json_file:
            json_file.write(json.dumps({"status": UNFINISHED_STATUS,
                                        "conversations": {}}))
    data_json = JSON_FILES.load_data_json(user, assignment)
    index = Utils.get_first_unfinished(JSON_FILES, data_json, assignment)
    return redirect(url_for("home", user=user, assignment=assignment,
                            index=index))


@app.route("/<user>/<assignment>/<index>/FinishAssignment",
           methods=["GET", "POST"])
def finish_assignment(user: str, assignment: str, index: str) -> Any:
    data_json = JSON_FILES.load_data_json(user, assignment)
    data_json["status"] = FINISHED_STATUS
    data_json["finished_time"] = round(datetime.now().timestamp(), 1)
    JSON_FILES.save_json(data_json, user, assignment)

    return redirect(url_for("set_assignment", user=user))


@app.route("/<user>/<assignment>/<index>/GoBackAction", methods=["POST"])
def go_back_action(user: str, assignment: str, index: str) -> Any:
    return return_next_conversation(
        user, assignment, index, str(max(int(index) - 1, 0)), request.form)


@app.route("/<user>/<assignment>/<index>/StopAction", methods=["POST"])
def stop_action(user: str, assignment: str, index: str) -> Any:
    Utils.save_lines_with_tags(JSON_FILES, user, assignment, index,
                               request.form)
    return redirect(url_for("set_assignment", user=user))


@app.route("/<user>/<assignment>/<index>/SaveDataToJson/<new_index>",
           methods=["GET", "POST"])
def save_data_to_json(user: str, assignment: str, index: str,
                      new_index: str) -> Any:
    return return_next_conversation(user, assignment, index, new_index,
                                    request.form)


@app.route("/<user>/<assignment>/<index>")
def home(user: str, assignment: str, index: str) -> Any:
    ind = int(index)
    is_hidden = False
    if "is_hidden" in request.args and request.args["is_hidden"] == "True":
        is_hidden = True

    annot_json = JSON_FILES.source_annot
    burst_id = JSON_FILES.assignments[user][assignment][0]

    if ind > len(annot_json[burst_id]):
        return Utils.get_prompt(CONVERSATION_INDEX_PROMPT)
    elif ind == len(annot_json[burst_id]):
        data_json = JSON_FILES.load_data_json(user, assignment)
        last_incomplete = Utils.get_first_unfinished(JSON_FILES, data_json,
                                                     assignment)
        not_annotated = UtilsStats.return_empty(JSON_FILES, user, assignment)

        return render_template("finish_assignment.html", user=user,
                               assignment=assignment, last_index=ind - 1,
                               last_incomplete=last_incomplete,
                               not_annotated=not_annotated)

    tagsets_json = JSON_FILES.tag_sets
    burst, tagset, _ = JSON_FILES.assignments[user][assignment]
    user_id, thread, conv = Utils.get_ids(annot_json, burst, ind)

    previous_strings, result_string, following_strings = Utils.get_strings(
        DATA_DIR, annot_json, burst, ind, SURROUNDING_LIMIT)

    loaded_tags = Utils.open_record(JSON_FILES, user, assignment, str(ind))
    manual = JSON_FILES.load_manual(JSON_DIR, tagsets_json[tagset]["manual"])

    return render_template(
        "home.html", result=result_string, previous=previous_strings,
        following=following_strings, source_ids=(user_id, thread, conv),
        tags=Utils.get_tags(JSON_FILES, tagset, tagsets_json),
        loaded_tags=loaded_tags, user=user, assignment=assignment, index=index,
        annots=len(annot_json[burst]), extended_tag=EXTENDED_TAG,
        manual=manual, hidden=is_hidden
    )


@app.route("/{}/stats/<user>/<assignment>".format(SUPERUSER_NAME),
           methods=["GET", "POST"])
def return_stat_data(user: str, assignment: str) -> Any:
    output, footer = UtilsStats.count_stats(JSON_FILES, user, assignment)
    return render_template("stats.html", user=user, assignment=assignment,
                           data=output, footer=footer)


@app.route("/{}".format(SUPERUSER_NAME), methods=["GET", "POST"])
def admin_start_page() -> Any:
    return redirect(url_for("admin_land_page"))


@app.route("/{}/admin".format(SUPERUSER_NAME), methods=["GET", "POST"])
def admin_land_page():
    return render_template("admin_land_page.html", superuser=SUPERUSER_NAME)


@app.route("/{}/admin/stats".format(SUPERUSER_NAME), methods=["GET", "POST"])
def admin_stats():
    return render_template("admin_data.html", superuser=SUPERUSER_NAME,
                           data=Utils.prepare_admin_data(
                               JSON_FILES, ACTIVE_STATUS, FINISHED_STATUS))


def generate_xslx_files():
    pass


@app.route("/{}/admin/xlsxGeneration".format(SUPERUSER_NAME),
           methods=["GET", "POST"])
def xlsx_generation():
    tagset_list, burst_list, users_list = Utils.prepare_xlsx_generation_data()
    return render_template("admin_xlsx_generation.html",
                           superuser=SUPERUSER_NAME,
                           tagset_list=tagset_list, burst_list=burst_list,
                           users_list=users_list)


@app.route("/{}/admin/xlsxGeneration/Generate".format(SUPERUSER_NAME),
           methods=["GET", "POST"])
def xlsx_generation_generate():
    Utils.generate_xslx_files(request.form)
    return render_template("admin_xlsx_generation_download.html",
                           superuser=SUPERUSER_NAME)


@app.route("/{}/admin/GetBackup".format(SUPERUSER_NAME),
           methods=["GET", "POST"])
def get_backup() -> Any:
    JSON_FILES.create_backup(True)
    index, _ = JSON_FILES.get_last_backup_info()
    return send_from_directory(
        directory=DOWNLOAD_DIR, filename="backup_{}.zip".format(index),
        as_attachment=True)


@app.route("/{}/admin/xlsxGeneration/DownloadCsv".format(SUPERUSER_NAME),
           methods=["GET", "POST"])
def get_annotation_csv() -> Any:
    return send_from_directory(
        directory=DOWNLOAD_DIR, filename=ANNOTATION_CSV, as_attachment=True)


@app.route("/{}/admin/xlsxGeneration/DownloadXlsx".format(SUPERUSER_NAME),
           methods=["GET", "POST"])
def get_annotation_xlsx() -> Any:
    return send_from_directory(
        directory=DOWNLOAD_DIR, filename=ANNOTATION_XLSX, as_attachment=True)


if __name__ == "__main__":
    app.run(debug=True)
