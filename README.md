## Structure of JSON files with annotation

* Each annotation task can be in three different states:
  * Active: No file with annotations is created
  * Unfinished: File with data about annotations is generated, but annotator did not manually set that annotation is finished
  * Finished: File with data about annotations is generated and annotator manually set that annotation is finished
* Generated files have following names: <hash_of_the_user>_<assignment_id>.json (Dict[str, Any])
* Structure of the file is as follows:
```
{ "status": str,
  "conversations": {
    <conversation_id>: [
      { <line_id>: <tag_id>, "ext"_<line_id>: <tag_id>, "additional_<line_id>: [<tag_id>, <tag_id>, ...]},
      [<annotation_start_time>, <annotation_start_time>, ...]
      [<annotation_finish_time>, <annotation_finish_time>, ...]
    ]
  }
}
```
* There are three types of annotations for each line:
  * <line_id>: primary tag for given line (see tagsets.json). Note: this can be 0 which specifies NO TAG.
  *  "ext"_<line_id>: secondary tag for given line (see tagsets.json). Note: this can be empty string
  * "additional_<line_id>: this specifies another primary tag given by other annotators. This can occure when more tags can be added to the given line and different annotators choose different tag (and at least one of them specifies that more than one tag should be assigned to this line). Note: this tag occures only in specific cases and is not mandatory.
* annotation_start_time and finish times are timestamps that specify when user started (and finished) to annotate given conversation.


## Structure of configuration JSON files

* users.json: Dict[str, List[Tuple[str, str]]]
  * Specifies the hash of annotator (for accessing the annotation system) and their human readable name
  ```
  { "users": [
      [<hash_of_the_annotator>, <annotator_nickname>]
    ]
  }
  ```

* assignments.json: Dict[str, Dict[str, Tuple[str, str, str]]]
  * Specifies assignments of annotation tasks to specific annotator and if the annotations are real data that should be used for ML. Each annotation task consists of burst number and tagset used for the annotation.
  ``` 
  { <hash_of_the_annotator>: {
      <assignment_id>: [<burst_id>, <tagset_id>, <True_if_annotation_represents_real_data>]
    }
  }
  ```
    
* tags.json: Dict[str, str]
  * Specifies id of tags and corresponding human readable form    
  ``` 
  { <tag_id>: <tag_descpription_in_human_readable_form> }
  ```

* tagsets.json: Dict[str, Dict[str, Any]]
  * Specifies tagsets that are used for annotations. Tagsets consist of two parts - one specifies the name of markdown file with instructions for given tagset. Second part specifies tag ids, which are used for annotations by this tagset. This is divided into two levels - primary tags (keys) and secondary tags for given primary tag (specified in value as ordered list).
  ``` 
  { <tagset_id>: [
      "tagset": [
        { <primary_tag_id>: [<secondary_tag_id>, <secondary_tag_id>, ...] }
      "manual": <name_of_file_with_instructions_for_this_tagset>
      ]
    ]
  }
  ``` 

* messenger_annotation.json: Dict[str, List[Tuple[str, str, str, bool, int]]]
  * Specifies concrete conversation selected for the bursts. It means that for each burst, the list of tuples with user_id, thread_id, conversation_id is provided.
  ``` 
  {
    <burst_id>: [
      [<user_id>, <thread_id>, <conversation_id>, <true_if_conversation_is_group_conversation>, <number_of_lines_in_conversation>]
    ]
  }


