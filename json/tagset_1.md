## ANOTAČNÍ MANUÁL: Rizikové jevy
#### (zkrácená verze  7. 1. 2021) 

#### **1. Agrese, násilí, obtěžování, nenávistné projevy**

\* platí pro mířené nadávky 
Je obsahem řádku něco z následujícího: | agresivní projevy/urážení/výhrůžky/pomluvy (s cílem ublížit) mířeny na účastníky konverzace ale i osoby mimo konverzaci | referování o násilném/agresivním chování svým nebo ostatních (včetně kyberšikany, obtěžování) | nenávistné/diskriminační/xenofobní projevy/nadávky zaměřené vůči lidem kvůli národnosti/etniku/barvě pleti/náboženství/sexualitě/váze (fat-shaming) | výzva k pomlouvání / násilí / agresi / atd. | 
* Agresí není: agresivní výraz spojený s humorem, smajlíky např.: “Ty jsi. Pica😂😂” (v ostatních případech, pokud vyjádření má potenciál ublížit příjemci, obtěžovat ho, je to verbální agrese). 
* NE sprosté slovo, které není adresné (nebo je jako vycpávka, např. pičo = voe -  to neanotujeme), pokud je ale “pičo” myšleno spíše adresně jako nadávka a má potenciál ublížit příjemci, obtěžovat ho, je to agrese. Důležité je směřování těch slov. 
* Pokud se agrese dotýká osob, které nejsou účastníci té konverzace, tak to kódujeme taky (např. učitelka je kráva, holka je vymaštěnost lvl 100+)
* Agrese není, když si říkají “nigga”, protože to není mířeno proti příslušníkům té menšiny (x když se baví, že půjdou k čongovi do obchodu - to je jasný rasismus). 
* Vulgarismus není vždy agresivní, je třeba se rozhodovat dle kontextu konverzace. Naopak použití “smajlíku” automaticky neznamená, že se nejedná o agresi.
* Kódujeme i cizojazyčné nadávky, pokud je anotátor rozezná. Nízká shoda nevadí, protože je budeme umět dodatečně odfiltrovat, kdyby to zlobilo.
* NE násilí v ustálených frázích, např. “je mi z toho špatně ještě jednou a jdu se oběsit”, “zabit ji je malo”. Držíme se záměru komunikace - záměru ublížit. Pokud o někom řeknu, že je na facku / na zabití, a myslím tím, že je prostě jaj, tak to nekóduju jako agresi. Ale pokud by to bylo, že mě tak štval, až jsem mu jednu vrazila, je to jasná agrese.
* Do agrese zařazujeme i urážení celých kategorií (třída, ČR, politické strany) a “veřejně známých” osob (politici, celebrity, atd.). 
* Implicitní rasismus je také agrese (“v mojem městě je úplně černo”), stejně tak agrese související s dehumanizací a dehonestací (“má iq tykve!” “je to ale gorila”). 
* Jako agresi nechápeme hněv a urážky konkrétních věcí (“zpíčená židle”) či aktů, agrese se týká osob  (“je to zkuřka” - ano, agrese; “šli chlastat” - ne, alkohol).
* Do agrese spadají i “mild” urážky, nápomocné může být pravidlo - řekl bych to já někomu, koho mám rád? Důležitý je záměr ublížit. Pokud si jsem jistý záměrem, ale ne slovem, mohu si pomoci otázkou - řekl bych to nějaké vážené osobě?
* Pomluvy a urážky - nezáleží, zda je osoba v právu a zda se “škaredě” baví o někom, kdo je “doopravdy zlý” - pokud je ve vyjadřování intence ublížit, očernit, ponížit, negativně hodnotit a je jasné, že daná osoba proti té druhé “něco má”, pak to kódujeme jako agresi.

#### **2. Problémy s duševním zdravím (mental health problems) a sebepoškozování**

Je obsahem něco z následujícího: referování/stěžování si na/zkušenost s přetrvávajícími/dlouhodobými psychickými problémy, depresí, úzkostmi, fóbií, paranoiemi, insomnií, poruchy příjmu potravy (anorexie, bulimie) v patologickém kontextu | diskuze ohledně sebepoškozování/sebevraždy | referování o léčbě, terapii |
* Nejsou to většinou krátkodobé pocity či afekty - pocity osamělosti, krátkodobý smutek, špatná nálada. Z kontextu musí být jasné, že to trvá už dlouho. Pokud to není jasné, tak nekódovat. 
* Našim cílem je poznat, že se baví o něčem, co sami chápou jako psychický problém. Ne jim dělat diagnózu psychického stavu.
* Při rozhodování může pomoci, že stav, který popisují, ovlivňuje jejich denní fungování - v takovém případě to určitě je problém s duševním zdravím.
* Udělat si test řádku - v jiném kontextu, indikuje řádek stále mental health problems? Dosaďte si to do konverzace o něčem jiném a pokud ten řádek neindikuje jev, tak nekódovat. 
* Být hodně konzervativní, kódovat jen explicitní zmínky. Když váháte - nekódovat. 
* Držíme se explicitních zmínek, snažíme se nechodit příliš “za text”
* Nejsou to: eating disorders a kategorie, které nám spadají do kategorií, které už máme podchycené. Ale pokud to je třeba deprese související s ED, tak by to tu podchycené být mělo. Nicméně pokud se baví o tom, že nejí, atd., tak to klasifikujeme jako Hubnutí, diety, proana obsahy.

#### **3. Alkohol a drogy (včetně alkoholu, nikotinu a dalších drog)**

Je obsahem řádku něco z následujícího: 
| popis zkušeností s drogami (cigarety, nikotin, marihuana, vodní dýmka, …) | referování o drogach/o tom, že je někdo pod jejich vlivem | domluva na konzumaci | shánění drog/poptávání drog | podporování/ospravedlnění/odůvodňování užívání drog | vyjádření přání/záměru konzumace | popis zkušeností s alkoholem/následky použití alkoholu | domluva na konzumaci alkoholu | vyjádření přání/záměru konzumace alkoholu | shánění alkoholu | odůvodňování/normalizace konzumace alkoholu | 
* POZOR: Za drogy považujeme také: léky zneužívané jako drogy| tabákové výrobky 

#### **4. Hubnutí a diety**

Je obsahem řádku něco z následujícího: 
| podpora v hubnutí | sdílení zkušeností s hubnutím | pozitivní hodnocení hubnutí | návod na dietu/ jak zhubnout/ jak být velmi hubený | zkušenost s dietou | vyjádření nespokojenosti s vlastní vahou (subjektivně vnímanou nadváhou) | sdílení/doporučení/hodnocení proana webů/skupin/článků/obsahů pokud to není jasně související s PPP |
* Hubnutí dětí není:  nezahrnuje ty, kteří chtějí mít svaly, patologické jevy spojené s PPP [to je mental health]
* Kategorie zahrnuje jak pozitivní, tak negativní jevy spojené s hubnutím.

#### **5. Sexuální obsah**

Je obsahem řádku něco z následujícího: 
| odkazy na pornostránky | povídání si o pornu, případně jeho obsahu | popis sexuálních zkušeností - i kdyby to bylo implicitní | informace o intimním životě | popis sexuálních praktik | otázka na sexuální praktiky | vyjádření přání/záměru provozovat pohlavní styk/sexuální praktiky | flirtování s explicitně sexuálním obsahem | referování o sexuálním životě ostatních | sexuální narážky (i s humorným podtextem) |
* Není pro nás důležité, zda sexuální praktiky a zkušenosti opravdu, o kterých se mluví doopravdy proběhly, důležité, že se o tom mluví 

#### **6. Cizí jazyk**

Vycházeli jsme z toho, že nebudeme anotovat konverzace vedeny v cizím jazyku s výjimkou situace, když je to vsuvka do komunikace v češtině. Vsuvkou rozumíme slovo v rámci české věty, nebo promluvu v rámci české konverzace (a to i v případě, že je promluva rozdělena např. do dvou řádku). Cizí jazyk je pro nás jakýkoli jiný jazyk než čeština (tedy i SK).

#### **Odpovědi a rozvíjení konverzace**

Odpověď se kóduje ve chvíli, kdy se to vztahuje k člověku, který o tom mluví - ať už je vlastníkem konverzace nebo někdo jiný [není to jen odpověď někomu - vychází to z jeho postoje, názoru, chování, společně to hodnotí, byl tomu exponován (viděl to, slyšel to, ovlivňovalo ho to jako osobu, pokud se někdo někoho dotazuje na jev a rozvíjí tím debatu - i když na to třeba nedostane kloudnou odpověď)], ale musí se to explicitně týkat jevu, o kterém se baví (pokud si nejste jistí, udělejte si prohazovací test - viz Problémy s duševním zdravím)

#### **Obecná pravidla** 

* Anotujeme spíše konzervativně, pokud nemáme kontext k něčemu, co podezíráme za konkrétní jev, tak to neanotujeme (např. rvačka se sourozenci a smajlíky).
* Anotujeme i negativní odpovědi, pokud splňují pravidla uvedená výše (týká se člověka, jevu, vychází z jeho chování / postoje / atd.), např. “jdem na cigo, jdeš tež?” - “smutné, já nekouřím”
