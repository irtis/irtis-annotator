## ANOTAČNÍ MANUÁL: PODPŮRNÉ INTERAKCE 
#### Verze 2. 23. 11, zkrácená verze  

### CO JE TO PODPORA (PODPŮRNÉ INTERAKCE ONLINE)?  

Podpůrné interakce online mezi vrstevníky, my se zaměříme konkrétně na **poskytování podpory** ve smyslu komunikace, o které můžeme předpokládat, že u příjemce vyvolá pocit/přesvědčení, že někomu na něm záleží,  je milován, vážen a oceňován, že někdo má o něj starost, je pro něj oporu, že mu někdo poskytne radu nebo užitečnou informaci, bude-li potřeba, pomůže mu, a také, že je součástí skupiny, s jejíž členy tráví čas a má společné plány.  
Pro každý typ podpory platí, že pokud výpověď v daném řádku nemá tento účel, neměla by být označená jako podpora.   
#### **1. Informační podpora**  
Je obsahem řádku něco z následujícího:  
 | dávání rad/tipů | učení | zpětné vazby | předávání znalostí/informací/zkušenosti, které druhý potřebuje a jsou mu nápomocné v řešení problému/pochopení situace, v které se ocitl|   
* Z kontextu konverzace musí být zřejmě, že informace jsou pro příjemce PODPORU v nějakém konkrétním jednání/akci/situaci | jsou nápomocné v řešení problému   
* INFORMAČNÍ PODPORA NENÍ: | Mluvení “jen tak” o něčem co se událo, co někdo četl nebo ví | informace předávány v rámci praktické nebo organizační DOMLUVY (např. čas a místo schůzky, poskytování kontaktních údajů pro další komunikaci, praktické domluvy) | uspokojení něčí zvědavosti |   
* POZOR! Praktické informace - pokud si druhý vyžádal, pokud je z kontextu jasné, že ji potřeboval, že mu pomohou ANO, je to případ podpory informací.   
* POZOR! Pokud po řádku obsahujícím informaci nasleduje “děkuji” apod., je to informační podpora   
Příklad: | **dávání rad** | “nejsou ale to asi nemusite ockovat a odcervovat, mely by byt v cajku”    
Příklad: | **předávání znalostí, informací nebo zkušenosti** | “ Do aj máš jenom ten talk + dneska se odevzdává ta matematika.”   
Příklad: | **učení** | “To b) máš btw špatně. Minimálně postup, výsledek ještě nevím”   
Návrh pravidla (zatím neplatí): IP NENÍ: pokyny/instrukce od učitele ve škole, ve skautu apod.   
Návrh pravidla (zatím neplatí):  dávání tipů: pouze vyžádány?   

#### **2. Emocionální podpora**  
Je obsahem řádku něco z následujícího:   
| vyjádření soucitu/empatie/vcítění se | sdílení emoce/pocitu (nikoli názoru nebo soudu/hodnocení | vyjádření ochoty naslouchat | povzbuzení (ve smyslu pomoci druhému cítit se sebevědomý a schopný v dané činnosti nebo situaci) | poskytnutí naděje/ujištění o úspěšnosti *budoucího* jednání | vyjádření sympatie/lásky/péče/starostlivosti/ | přání | vyjádření radosti s budoucího/minulého setkání nebo zklamání z nesetkání | oslovení typu “broučku/lásko/ apod.) | vyjádření, že druhý je pro někoho důležitý | uklidnění někoho, že vše bude v pořádku/dobrý | utěšení   
* EP není: obraty „Máš se?“ pro zahájení/udržení konverzace, mimo kontext starostlivosti, lásky | Vyjádření díků za pomoc   
* PŘÁNÍ NENÍ  | loučení typu “hezký večer” | “užij si”   
Příklad: | **vyjádření soucitu/empatie/vcítění se/pochopení** |   
Příklad: | **sdílení emoce/pocitu** |   
Příklad: | **povzbuzení** | “Je to mega ez    
Příklad: | **povzbuzení** | “Ester, moc mě mrzí, že ti to řekla. Hrozně moc :( Chci říct, že nemáš široké boky”   
Příklad: | **poskytnutí naděje/ujištění o úspěšnosti budoucího jednání** | “Když budeš chtít, tak to vyjde😂”    
Příklad: | **vyjádření ochoty naslouchat** | “Copak se děje Ester?” / „Jak ti je?“    
Příklad: | **vyjádření sympatie** | „Tebe miluju <3“ | „Mám Tě ráda“   
Příklad: | **vyjádření radosti s budoucího setkání** | “Budu se těšit, až se uvidíme”    
#### **3. Začlenění**   
Je obsahem řádku něco z následujícího: | pozvání k skupině | pozvání k zapojení se k nějaké společné aktivitě/plánu/setkání | stvrzení podobnosti/sounáležitosti/společné minulosti/příslušnosti k dané skupině | otázka, zda druhá osoba “někde bude” (“někde” musí být i ten, kdo se ptá) | podmínění svého zapojení/účastí zapojením druhého    
* ZAČLENĚNÍ NENÍ: Hlášení/výzvy botů z online her (Lucinka právě odehrál(a)! Jste na řadě!) | Diskuze o praktických aspektech společného plánu | Odpověď na začlenění (“Jasně, jdu!”) | “Vtírání se” (“A se mnou počítáte?”)   
Příklad: | **pozvání k skupině** |    
Příklad: | **pozvání k zapojení se k nějaké společné aktivitě/plánu** | “Jdeme ten fortnite?”     
Příklad: | **stvrzení podobnosti/sounáležitosti/ příslušnosti k dané skupině** |   
Příklad: | stvrzení společné minulosti | “Vždycky když to slyším tak si vzpomenu na tebe”   
* návrh pravidla (zatím neplatí), vždy první začleňující výrok, další se budou týkat praktických aspektů    
#### **4. Uznání**   
Je obsahem řádku něco z následujícího: | pochvala/pozitivní hodnocení příjemce/jeho schopností/výkonu/majetku/vzhledu/vlastností/osobnosti/jednání | poznámka vyjadřující obdiv nebo respekt | ujištění ohledně správnosti uvažování/jednání/pocitu (ve smyslu práva na to, se tak cítit, jednat) |    
* UZNÁNÍ NENÍ: vyjádření díků za pomoc (ALE pokud obsahuje pozitivní hodnocení příjemce, tak ANO, viz Příklad)   
Příklad:  | **pochvala/pozitivní hodnocení** | “Jsi dobrá”   
Příklad:  | **ujištění ohledně správnosti uvažování/jednání** | “Myslím, že na to máš právo.”   
Příklad: | **poděkování s uznáním** | “Děkuji, jsi zlatá!”   
#### **5. Nabídka pomoci**   
Je obsahem  řádku  něco z následujícího: | nabídka pomoci/pomoc s prováděním úkolu/činnosti/řešení situace | neodmítnutí/souhlas s poskytnutím pomoci někomu/půjčením/poskytnutím věci | někdo se hlásí/nabízí k provedení úkolu nebo jeho části   

Příklad: | **pomoc s prováděním úkolu** | “I told him that I was fine” (řádek 272)   
Příklad: | **nabídka pomoci** | “Až pojedu kolem ti pisnu a hodim ti ho”   

#### **6. Cizí jazyk**

Vycházeli jsme z toho, že nebudeme anotovat konverzace vedeny v cizím jazyku s výjimkou situace, když je to vsuvka do komunikace v češtině. Vsuvkou rozumíme slovo v rámci české věty, nebo promluvu v rámci české konverzace (a to i v případě, že je promluva rozdělena např. do dvou řádku). Cizí jazyk je pro nás jakýkoli jiný jazyk než čeština (tedy i SK).

#### **Obecná pravidla**  

* Při rozhodování zohledňujeme také implicitní významy.    
* Smajlíky (emojis) na řádku samostatně neanotujeme, nicméně emojis nám mohou pomáhat při interpretaci.   
* Promluva musí být myšlena vážně, ne z legrace, ironie.    
* Zákaznická podpora NE/komunikace s firmou nebo úřadem NE     
